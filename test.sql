-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 08 Juin 2015 à 13:30
-- Version du serveur :  5.6.24
-- Version de PHP :  5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id`, `name`, `author`, `created_at`, `updated_at`) VALUES
(3, 'hiep3336666666666', 'hiepcdit7777777777888', '2015-05-28 19:02:15', '2015-06-01 20:29:57'),
(4, 'hiepcdit', 'hiepcdit', '2015-05-29 12:49:14', '2015-05-29 12:49:14'),
(5, 'Thi ha', 'nguyen thi ha', '2015-05-29 12:49:36', '2015-05-29 12:49:36'),
(6, 'hiep', 'hiep', '2015-05-29 12:50:30', '2015-05-29 12:50:30'),
(7, '', '', '2015-05-29 07:00:00', '2015-05-29 13:05:57'),
(8, 'duc hiep ', 'duc hiep pro', '2015-05-29 07:00:00', '2015-05-29 13:06:46'),
(9, 'Duc hiep', 'duc hiep 3', '2015-05-29 13:14:10', '2015-05-29 13:14:10'),
(10, 'hiep', 'hiep', '2015-05-31 13:14:35', '2015-05-29 13:14:35'),
(11, 'Dddddd', 'dddd', '2015-05-31 16:43:57', '2015-05-31 16:43:57'),
(12, 'hiepcdit', 'Duc hiep Pro', '2015-05-31 16:44:16', '2015-05-31 16:44:16'),
(13, 'hiepcdit5', 'hiepduc', '2015-06-01 17:00:00', '2015-06-01 20:01:54'),
(14, '111111', '1111111', '2015-06-01 17:00:00', '2015-06-01 20:02:29'),
(15, '222222222', '2222222222222', '2015-06-01 20:03:37', '2015-06-01 20:03:37');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_05_28_055256_create_articles_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `page_name`, `content`) VALUES
(1, 'Te nan xa hoi', 'Chung ta dang song trong mot xa hoi day nhung doi tra, nhung khong dau do quanh chung ta van con nhieu nguoi tot, nhieu manh doi kho khan nhung van tim doc luc, cong viec, va muc dich moi.'),
(6, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(7, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(8, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(14, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(15, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(16, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(17, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(18, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui'),
(19, 'Tro ve nha', 'Hanh phuc doi voi ban la gi, rieng toi, mot ngay lam viec, tot tro ve co mot gia dinh, co ai ai do dang doi minh, co mot noi de co the ve, thi da la hanh phuc va hanh phuc khi toi thuc hien mot dieu gi do tot dep cho nhung nguoi xung quanh, lam cho ho vui');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `name`, `status`) VALUES
(1, 'Hoc lap trinh php', 1),
(2, 'Ky nang lap trinh php', 1),
(3, 'Nhung kien thuc can thiet khi hoc LT', 1),
(4, 'Hoc lap trinh javascript', 1),
(5, 'Vi sao lai chon lap trinh php', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user2`
--

CREATE TABLE IF NOT EXISTS `user2` (
  `PersonID` int(11) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(41, '', 'hiepcdit@gmail.com', '123456', NULL, '2015-06-01 08:23:50', '0000-00-00 00:00:00'),
(42, '', 'hiepc3333dit@gmail.com', 'kkkkkkk', NULL, '2015-06-01 09:06:07', '0000-00-00 00:00:00'),
(44, '', 'hiepcdit4@gmail.com', 'kkkkkkk', NULL, '2015-06-01 09:07:28', '0000-00-00 00:00:00'),
(45, '', 'hiepcdit7@gmail.com', '''''''select', NULL, '2015-06-01 09:08:44', '0000-00-00 00:00:00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
