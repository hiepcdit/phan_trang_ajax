//JavaScript Document

//create plugin
(function($) {
	$.fn.zPaging =	function(options){
		console.log('Hi zPaging');

		//================================================
		//Cac gia tri mac dinh
		//================================================
		var defaults = {
			"rows": "#rows",
			"pages":"#pages",
			"items": 2,
			"height": 29,
			"currentPage" :1,
			"total":0,
			"btnPrevious" : ".goPrevious",
			"btnNext":".goNext",
			"txtCurrentPage":"#currentPage",
			"PageInfo":".pageInfo"
		};

		options = $.extend(defaults, options);
		
		//================================================
		//Cac bien se su dung trong Plugin
		//================================================
		var rows 			= $(options.rows);
		// alert(defaults.rows);
		var pages 			= $(options.pages);
		var btnPrevious 	= $(options.btnPrevious);
		var btnNext 		= $(options.btnNext);
		var txtCurrentPage 	= $(options.txtCurrentPage);
		var labPageInfo	 	= $(options.PageInfo);
		var aRows 			= '';

		//================================================
		//Cac ham thi hanh
		//================================================
		_constract();
		setRowsHeight();


		//================================================
		//Ham khoi doi
		//================================================
		function _constract(){
			//Lay tong so trang
			$.ajax({
				url: 'files/file.php?type=count&items='+options.items,
				type: 'GET',
				dataType: 'json',
				//data: {param1: 'value1'},
			})
			.done(function(data) {
				options.total = data.total;
				pageInfo();
				loadData(options.currentPage);
				//Set trang hien tai
				setCurrentPage(options.currentPage);

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});//end $.ajax
			
			//Gan su kien vao btnPrevious - btnNext - txtCurrentPage
			

			//khi co click vao Previous
			btnPrevious.on('click', function(e) {
				goPrevious();
				e.stopImmediatePropagation();
			});

			//khi co click vao Next
			btnNext.on('click', function() {
				goNext();
				e.stopImmediatePropagation();
			});

			//Khi co nhap gia tri cho input
			txtCurrentPage.on('keyup', function(e) {
				var key = e.which || e.keyCode;
				if(key == 13){
					var currentPageValue = parseInt($(this).val());
					if(isNaN(currentPageValue) 
								|| currentPageValue <= 0 
								|| currentPageValue > options.total)
					{
							alert('Gia tri nhap khong phu hop!');
					}else{
						options.currentPage = currentPageValue;
						loadData(currentPageValue);
						pageInfo();

					}//end if isNaN
					
				}//end if

			});//end txtCurrentPage

		}//end _constract

		//================================================
		//Ham xu ly khi click vao btnPrevious
		//================================================
		function goPrevious(){
			console.log('Previous');
			if(options.currentPage != 1){
				var p = options.currentPage - 1;
				loadData(p);
				setCurrentPage(p);
				options.currentPage = p;
				//alert(options.currentPage);
				pageInfo();
			}
		}

		//================================================
		//Ham xu ly khi click vao btnNext
		//================================================
		function goNext(){
			console.log('Previous');
			if(options.currentPage != options.total && options.total != 0){
				var p = options.currentPage + 1;
				loadData(p);
				setCurrentPage(p);
				options.currentPage = p;
				//alert(options.currentPage);
				pageInfo();
			}

		}

		//================================================
		//Ham xu ly gan gia tri vao
		//trong o texbox currentPage
		//================================================

		function setCurrentPage(value){
			if(options.total == 0){
				value = 0 ;
			}
			txtCurrentPage.val(value);
		}

		//================================================
		//Ham hien thi thong tin phan trang
		//================================================

		function pageInfo() {
			console.log(options.total);
			// body...
			if(options.total == 0){
				options.currentPage = 0;
			}
			labPageInfo.text("Page " + options.currentPage 
								  	 + " of " 
								  	 + options.total);
		}

		//================================================
		//Thiet lap chieu cao cua ul#rows
		//================================================

		function setRowsHeight () {
			var ulHeight = (options.items * options.height) + "px";
			rows.css({"height":ulHeight,"overflow": 'hidden'});
		}

		//================================================
		//Ham load thong tin trong database dua vao #rows
		//================================================

		function loadData (page) {
			console.log("loadData" + page);
			$.ajax({
				url: 'files/file.php?type=list',
				type: 'POST',
				dataType: 'json',
				cache: false,
				data: { 
						"items"			: options.items,
						"currentPage"	: page
					  },
			})
			.done(function(data) {
				//console.log(data);
				if (data.length > 0) {
					rows.empty();
					$.each(data,function(index, value) {
						var str = '<li item-id ="' + value.id +'">' 
						+ value.id + '- ' + value.name 
						+ '<a href ="#"> Delete </a>'
						+ '</li>';
						rows.append(str);
					});

					//lay tap hop cac the <a> nam ul#row
					aRows = options.rows + " li a";
					console.log(aRows);
					$(aRows).on('click', function(e) {
						deleteItem(this);
					});
				};
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		}

		//================================================
		//Ham xoa di mot dong trong #rows
		//================================================

		function deleteItem (obj) {
			var parent = $(obj).parent();
			var itemID = $(parent).attr('item-id');
			var lastID = $(rows).children(':last').attr("item-id");
			console.log(lastID);
			//An va xao li duoc click
			$(parent).fadeOut({
				durarion: 100,
				done:function () {
					//Xoa 1 dong trong database
					$.ajax({
						url: 'files/file.php?type=delete&id=' + itemID,
						type: 'GET'
					})
					if(itemID = lastID && $(rows).children().length == 1){
						options.currentPage = options.currentPage - 1;
						
					}//end if
					//load lai sau khi xoa
					_constract();
					pageInfo();

					$(this).remove();

				}//end ajax
			});

			//load them 1 dong phia duoi dong duoc xoa
			$.ajax({
				url: 'files/file.php?type=one&id=' + lastID,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(data) {
				if(data != false ){
					var str = '<li item-id ="' + data.id +'">' 
							+ data.id + '- ' + data.name 
							+ '<a href ="#"> Delete </a>'
							+ '</li>';
					str = $(str).hide().appendTo(rows);
					$(str).fadeIn();

					//lay tap hop cac the <a> nam ul#row
					aRows = options.rows + " li a";
					$(aRows).on('click', function(e) {
						deleteItem(this);
					});
				}//end if
				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});

		}
	}//end plugin zPaging.
})(jQuery);

$(document).ready(function(e){
	//dua gia tri vao
	var obj = {"items":2} ;
	$("#paging").zPaging(obj);
});