<!DOCTYPE html>
<html lang="en">
<head>
	<title>Document</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style.css">
	<script src="js/jquery-2.0.3.js" type="text/javascript" charset="utf-8" async defer></script>
	<script src="js/paging.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>


	<div id="wrrap">
		
		<div id="header">
			<h1>Header</h1>
		</div><!-- End #header -->
		<div id="content">
			<div id="left">
				<h2>Left Content</h2>
				<div id="paging">
					<ul id="rows">
						<!-- <li item-id="1">
							1 - PHP an MySQL web Development
							<a href="#">delete</a>
						</li>
						<li item-id="2">
							2 - PHP an MySQL web Development
							<a href="#">delete</a>
						</li>
						<li item-id="3">
							3 - PHP an MySQL web Development
							<a href="#">delete</a>
						</li>
						<li item-id="4">
							4 - PHP an MySQL web Development
							<a href="#">delete</a>
						</li> -->
					</ul>
					<ul id="pages">
						<li class="pageInfo">Page 1 of 6</li>
						<li class="goPrevious"><< Previous</li>
						<li><input type="text"  value="1" id="currentPage" class="currentPage"></li>
						<li class="goNext">Next >></li>
					</ul>
				</div><!-- end #paging -->
			</div><!-- end #left -->
			<div id="right">
				<h2>Right Content</h2>
				<p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>
			</div><!-- end #right -->

		</div><!-- End #content -->
		<div id="footer">
		<h3>Footer</h3>
		</div><!-- End #footer -->
	</div><!-- End #wrrap -->
	
</body>
</html>