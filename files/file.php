<?php 
	require_once("connect.php");

	$type = (string)$_GET['type'];
	
	//================================================
	//Lay tong so trang trong database
	//================================================

	if($type == 'count'){
		//tinh tong so trang
		$items = (int)$_GET['items'];
		$sql = 'SELECT COUNT(id) as count
				FROM `products`
				WHERE `status` = 1 ';
		$result = mysqli_query($connect,$sql);
		$row = mysqli_fetch_array($result);
		$totalItem = $row['count'];
		$total = array('total'=>0);
		$totalPage = ceil($totalItem/$items);
		$total['total'] = $totalPage;
		echo json_encode($total);
	}//end if

	//================================================
	//Lay du lieu trong database load vao #rows
	//================================================
	if($type == 'list'){

		$items 			= (int)$_POST['items'];
		$currentPage 	= (int)$_POST['currentPage'];
		$offset			= ($currentPage - 1) * $items;
		//LIMIT offset , items
		//page: 1 => 0 , 4
		//page: 2 => 4 , 4
		//page: 3 => 8 , 4

		$sql = 	' SELECT `id`,`name`
				  FROM `products`
				  WHERE `status` = 1
				  ORDER BY `id` ASC
				  LIMIT ' . $offset . ',' .$items;
		$result = mysqli_query($connect,$sql);
		$data = array();
		while($row = mysqli_fetch_array($result,MYSQL_ASSOC)){
			$data[] = $row;
		}

		/*echo '<pre>';
		print_r($data);
		echo '</pre>';*/

		echo json_encode($data);
	}

	//================================================
	//Lay 1 dong dong tiep theo add vao page hien tai
	//================================================

	if($type == 'one'){

		$lastID		= (int)$_GET['id'];
		
		$sql = 	" SELECT `id`,`name`
				  FROM `products`
				  WHERE `status` = 1 AND `id` > '{$lastID}'
				  ORDER BY `id` ASC
				  LIMIT 1 ";
		$result = mysqli_query($connect,$sql);
		$data = array();
		$data = mysqli_fetch_array($result,MYSQL_ASSOC);
		
		echo json_encode($data);
	}

	//================================================
	//Delete phan tu duoc click
	//================================================

	if($type == 'delete'){

		$id= (int)$_GET['id'];
		$sql = 	" DELETE FROM `products`
				  WHERE `id` = '{$id}' 
				  LIMIT 1 " ;
		mysqli_query($connect,$sql);		
	}

 ?>